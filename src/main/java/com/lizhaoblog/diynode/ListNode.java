/*
 * Copyright (C), 2015-2018
 * FileName: ListNode
 * Author:   zhao
 * Date:     2018/9/11 15:50
 * Description: 链表用到的数据结构
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.diynode;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈链表用到的数据结构〉
 *
 * @author zhao
 * @date 2018/9/11 15:50
 * @since 1.0.1
 */
public class ListNode {
  public int val;
  public ListNode next;

  public ListNode(int x) {
    val = x;
    next = null;
  }

  public List<Integer> toArray() {
    List<Integer> list = new ArrayList<>();

    ListNode curNode = this;

    do {
      list.add(curNode.val);
      curNode = curNode.next;
    } while (curNode != null);

    return list;
  }

}
