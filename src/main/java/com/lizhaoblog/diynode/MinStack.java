/*
 * Copyright (C), 2015-2018
 * FileName: MinStack
 * Author:   zhao
 * Date:     2018/9/13 11:47
 * Description: 最小栈
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.diynode;

import com.lizhaoblog.hundredtwo.Solution155;

import java.util.Stack;

/**
 * 〈一句话功能简述〉<br>
 * 〈最小栈〉
 *
 * @author zhao
 * @date 2018/9/13 11:47
 * @see Solution155
 * @since 1.0.1
 */
public class MinStack {

  private Stack<Integer> stack;
  private Stack<Integer> minStack;

  /**
   * initialize your data structure here.
   */
  public MinStack() {
    stack = new Stack<>();
    minStack = new Stack<>();
  }

  public void push(int x) {
    stack.push(x);
    if (minStack.empty()) {
      minStack.push(x);
    } else {
      if (minStack.peek() >= x) {
        minStack.push(x);
      }
    }
  }

  public void pop() {
    if (stack.empty()) {
      return;
    }
    Integer pop = stack.pop();
    if (!minStack.empty() && pop != null) {
      Integer peek = minStack.peek();
      if (peek.equals(pop)) {
        minStack.pop();
      }
    }
  }

  public int top() {
    Integer result = stack.peek();
    //    if (result == null) {
    //      throw new Exception("stack is empty");
    //    }

    return result;
    //    return result == null ? null : result;
  }

  public int getMin() {
    return minStack.peek();
  }

}
