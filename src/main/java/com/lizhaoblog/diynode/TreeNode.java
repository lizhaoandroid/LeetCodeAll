/*
 * Copyright (C), 2015-2018
 * FileName: TreeNode
 * Author:   zhao
 * Date:     2018/9/7 18:10
 * Description: 二叉树节点
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.diynode;

/**
 * 〈一句话功能简述〉<br>
 * 〈二叉树节点〉
 *
 * @author zhao
 * @date 2018/9/7 18:10
 * @since 1.0.1
 */
public class TreeNode {
  public int val;
  public TreeNode left;
  public TreeNode right;

  public TreeNode(int x) {
    val = x;
  }

}
