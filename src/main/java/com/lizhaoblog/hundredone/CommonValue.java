/*
 * Copyright (C), 2015-2018
 * FileName: CommonValue
 * Author:   zhao
 * Date:     2018/9/6 15:26
 * Description: 默认值
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.hundredone;

/**
 * 〈一句话功能简述〉<br>
 * 〈默认值〉
 *
 * @author zhao
 * @date 2018/9/6 15:26
 * @since 1.0.1
 */
public class CommonValue {
  public static final boolean NOT_ANSWER = true;
}
