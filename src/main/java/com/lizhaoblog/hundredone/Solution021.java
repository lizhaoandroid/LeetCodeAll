/*
 * Copyright (C), 2015-2018
 * FileName: Solution021
 * Author:   zhao
 * Date:     2018/7/31 17:30
 * Description: 合并两个有序列表
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.hundredone;

import com.lizhaoblog.diynode.ListNode;

/**
 * 〈一句话功能简述〉<br>
 * 〈合并两个有序列表〉
 *
 * @author zhao
 * @date 2018/7/31 17:30
 * @since 1.0.1
 */
public class Solution021 {
    /**************************************
     * 题目
     * 将两个有序链表合并为一个新的有序链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。
     *
     * 示例：
     *
     * 输入：1->2->4, 1->3->4
     * 输出：1->1->2->3->4->4
     **************************************/

    /**************************************
     *
     * 想法：
     *
     *      声明结果链表C，同时遍历2个链表A和B，
     *      比对2个链表当前值的数据大小，
     *      如果A比B小，就往结果链表C中赋值，然后A链表往前走
     *      反之则B链表往前走
     *
     *
     * 我的做法
     * 超过99%的测试案例
     *
     * ***********************************/
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        }
        if (l2 == null) {
            return l1;
        }

        ListNode result = new ListNode(0);

        ListNode cur = result;
        ListNode cur1 = l1;
        ListNode cur2 = l2;

        if (cur1.val < cur2.val) {
            cur.val = cur1.val;
            cur1 = cur1.next;
        } else {
            cur.val = cur2.val;
            cur2 = cur2.next;
        }

        while (cur1 != null || cur2 != null) {
            if (cur1 == null) {
                cur.next = cur2;
                return result;
            }
            if (cur2 == null) {
                cur.next = cur1;
                return result;
            }

            if (cur1.val < cur2.val) {
                cur.next = cur1;
                cur1 = cur1.next;
            } else {
                cur.next = cur2;
                cur2 = cur2.next;
            }
            cur = cur.next;
        }

        return result;
    }

    public ListNode better(ListNode l1, ListNode l2) {
        ListNode result = new ListNode(0);
        ListNode tmp = result;
        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                tmp.next = l1;
                l1 = l1.next;
                tmp = tmp.next;
            } else {
                tmp.next = l2;
                l2 = l2.next;
                tmp = tmp.next;
            }
        }
        if (l1 == null) {
            tmp.next = l2;
        }
        if (l2 == null) {
            tmp.next = l1;
        }
        return result.next;
    }

}



