/*
 * Copyright (C), 2015-2018
 * FileName: Solution058
 * Author:   zhao
 * Date:     2018/9/5 10:16
 * Description: 58. 最后一个单词的长度
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.hundredone;

/**
 * 〈一句话功能简述〉<br>
 * 〈58. 最后一个单词的长度〉
 *
 * @author zhao
 * @date 2018/9/5 10:16
 * @since 1.0.1
 */
public class Solution058 {

  /**************************************
   * 题目
   给定一个仅包含大小写字母和空格 ' ' 的字符串，返回其最后一个单词的长度。

   如果不存在最后一个单词，请返回 0 。

   说明：一个单词是指由字母组成，但不包含任何空格的字符串。

   示例:

   输入: "Hello World"
   输出: 5
   **************************************/

  /**************************************
   *
   * 想法：
   *      判断字符串为空
   *      spilt(),然后取出最后一个数组
   * 我的做法：
   *      超过73.31%的测试案例
   *  总结：
   *      没有判断字符串 " "
   * ***********************************/
  public int lengthOfLastWord(String s) {
    if (s.trim().length() == 0) {
      return 0;
    }

    String[] split = s.split(" ");
    return split[split.length - 1].length();
  }

  /**************************************
   * 比我好的答案
   * ***********************************/
  public int better(String s) {
    return s.trim().length() - s.trim().lastIndexOf(" ") - 1;
  }

}
