/*
 * Copyright (C), 2015-2018
 * FileName: Solution28
 * Author:   zhao
 * Date:     2018/7/30 11:12
 * Description: strStr()
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.hundredone;

/**
 * 〈一句话功能简述〉<br>
 * 〈strStr()〉
 *
 * @author zhao
 * @date 2018/7/30 11:12
 * @since 1.0.1
 */
public class Solution28 {

  /*********************
   * 题目
   * 给定一个 haystack 字符串和一个 needle 字符串，在 haystack 字符串中找出 needle 字符串出现的第一个位置 (从0开始)。如果不存在，则返回  -1。
   *
   * 示例 1:
   *
   * 输入: haystack = "hello", needle = "ll"
   * 输出: 2
   * 示例 2:
   *
   * 输入: haystack = "aaaaa", needle = "bba"
   * 输出: -1
   * 说明:
   *
   * 当 needle 是空字符串时，我们应当返回什么值呢？这是一个在面试中很好的问题。
   *
   * 对于本题而言，当 needle 是空字符串时我们应当返回 0 。这与C语言的 strstr() 以及 Java的 indexOf() 定义相符。
   *
   *
   *
   */

  /**************************************
   * 我的做法
   * 超过82.66%的测试案例
   *
   * 想法：
   * 从左到右，从0~haystack.length不同位置截取needle长度的字符串
   * 将截取出来的字符串和needle对比
   * ***********************************/
  public int strStr(String haystack, String needle) {

    if (needle.length() == 0) {
      return 0;
    }

    for (int i = 0; i < haystack.length(); i++) {
      if (haystack.length() - needle.length() - i < 0) {
        return -1;
      }
      String sub = haystack.substring(i, i + needle.length());
      if (needle.equals(sub)) {
        return i;
      }

    }
    return -1;
  }

  /**
   * @param haystack 源字符串
   * @param needle   要查找的字符串
   * @return 返回index
   */
  public int better(String haystack, String needle) {
    return haystack.indexOf(needle);
  }

}
