/*
 * Copyright (C), 2015-2018
 * FileName: SolutionExample
 * Author:   zhao
 * Date:     2018/7/31 15:34
 * Description: 默认
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.hundredone;

/**
 * 〈一句话功能简述〉<br>
 * 〈默认〉
 *
 * @author zhao
 * @date 2018/7/31 15:34
 * @since 1.0.1
 */
public class SolutionExample {
    /**************************************
     * 题目

     **************************************/

    /*************************************
     想法：

     我的做法
     超过  %的测试案例
     时间复杂度/空间复杂度：  /
     代码执行过程：

     总结：

     ************************************/
    public void example() {

    }

    /**************************************
     * 比我好的答案 better
     * ***********************************/
    public void better() {
    }
}
