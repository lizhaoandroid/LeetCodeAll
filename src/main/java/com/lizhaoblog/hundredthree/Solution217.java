/*
 * Copyright (C), 2015-2018
 * FileName: Solution217
 * Author:   zhao
 * Date:     2018/9/25 14:58
 * Description: 217. 存在重复元素
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.hundredthree;

import java.util.Arrays;
import java.util.HashMap;

/**
 * 〈一句话功能简述〉<br>
 * 〈217. 存在重复元素〉
 *
 * @author zhao
 * @date 2018/9/25 14:58
 * @since 1.0.1
 */
public class Solution217 {
  /**************************************
   * 题目

   给定一个整数数组，判断是否存在重复元素。

   如果任何值在数组中出现至少两次，函数返回 true。如果数组中每个元素都不相同，则返回 false。

   示例 1:

   输入: [1,2,3,1]
   输出: true
   示例 2:

   输入: [1,2,3,4]
   输出: false
   示例 3:

   输入: [1,1,1,3,3,4,3,2,4,2]
   输出: true
   **************************************/

  /**************************************
   *
   * 想法：
   *    1. HashMap
   *        key数组中的值，value为1
   *        根据key，能取出数据则为true
   *
   * 我的做法
   *      超过  %的测试案例
   *
   * 代码执行过程：
   *
   * 总结：
   *
   * ***********************************/
  public boolean containsDuplicate(int[] nums) {
    HashMap<Integer, Integer> map = new HashMap<>();
    for (int n : nums) {
      Integer integer = map.get(n);
      if (integer != null) {
        return true;
      }
      map.put(n, 1);
    }

    return false;
  }

  /**************************************
   * 比我好的答案 better
   * ***********************************/
  public boolean better(int[] nums) {
    if (nums.length <= 1) {
      return false;
    }
    Arrays.sort(nums);
    for (int i = 0; i < nums.length - 1; i++) {
      if (nums[i] == nums[i + 1]) {
        return true;
      }
    }
    return false;

  }
}
