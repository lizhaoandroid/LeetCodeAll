/*
 * Copyright (C), 2015-2018
 * FileName: Solution237
 * Author:   zhao
 * Date:     2018/9/27 18:57
 * Description: 237. 删除链表中的节点
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.hundredthree;

import com.lizhaoblog.diynode.ListNode;

import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈237. 删除链表中的节点〉
 *
 * @author zhao
 * @date 2018/9/27 18:57
 * @since 1.0.1
 */
public class Solution237 {

  /**************************************
   * 题目
   请编写一个函数，使其可以删除某个链表中给定的（非末尾）节点，你将只被给定要求被删除的节点。

   现有一个链表 -- head = [4,5,1,9]，它可以表示为:

   4 -> 5 -> 1 -> 9
   示例 1:

   输入: head = [4,5,1,9], node = 5
   输出: [4,1,9]
   解释: 给定你链表中值为 5 的第二个节点，那么在调用了你的函数之后，该链表应变为 4 -> 1 -> 9.
   示例 2:

   输入: head = [4,5,1,9], node = 1
   输出: [4,5,9]
   解释: 给定你链表中值为 1 的第三个节点，那么在调用了你的函数之后，该链表应变为 4 -> 5 -> 9.
   说明:

   链表至少包含两个节点。
   链表中所有节点的值都是唯一的。
   给定的节点为非末尾节点并且一定是链表中的一个有效节点。
   不要从你的函数中返回任何结果。
   **************************************/

  /**************************************
   *
   * 想法：
   *    遍历链表，修改next指向
   * 我的做法
   *      超过  %的测试案例
   *
   * 代码执行过程：
   *
   * 总结：
   *
   * ***********************************/
  public void deleteNode(ListNode head, ListNode node) {

    if (head.val == node.val) {
      head = head.next;
      return;
    }

    ListNode cur = head;
    ListNode pre = head;

    while (cur != null) {
      if (cur.val == node.val) {
        pre.next = cur.next;
      }
      pre = cur;
      cur = cur.next;
    }

  }

  /**************************************
   * 比我好的答案 better
   * ***********************************/
  public void better() {
  }
}
