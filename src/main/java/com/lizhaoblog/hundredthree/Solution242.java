/*
 * Copyright (C), 2015-2018
 * FileName: Solution242
 * Author:   zhao
 * Date:     2018/9/27 11:20
 * Description: 242. 有效的字母异位词
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.hundredthree;

import java.util.Arrays;

/**
 * 〈一句话功能简述〉<br>
 * 〈242. 有效的字母异位词〉
 *
 * @author zhao
 * @date 2018/9/27 11:20
 * @since 1.0.1
 */
public class Solution242 {
  /**************************************
   * 题目
   给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的一个字母异位词。

   示例 1:

   输入: s = "anagram", t = "nagaram"
   输出: true
   示例 2:

   输入: s = "rat", t = "car"
   输出: false
   说明:
   你可以假设字符串只包含小写字母。

   进阶:
   如果输入字符串包含 unicode 字符怎么办？你能否调整你的解法来应对这种情况？
   **************************************/

  /**************************************
   *
   * 想法：
   *    一、把字母进行排序
   *
   *
   * 我的做法
   *      超过  %的测试案例
   *
   * 代码执行过程：
   *
   * 总结：
   *
   * ***********************************/
  public boolean isAnagram(String s, String t) {
    if (s.length() != t.length()) {
      return false;
    }

    char[] charsS = s.toCharArray();
    char[] charsT = t.toCharArray();
    Arrays.sort(charsS);
    Arrays.sort(charsT);
    for (int i = 0; i < charsS.length; i++) {
      if (charsS[i] != charsT[i]) {
        return false;
      }

    }
    return true;
  }

  /**************************************
   * 比我好的答案 better
   * ***********************************/
  public void better() {
  }

}
