/*
 * Copyright (C), 2015-2018
 * FileName: Solution121
 * Author:   zhao
 * Date:     2018/9/9 17:59
 * Description: 121. 买卖股票的最佳时机
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.hundredtwo;

/**
 * 〈一句话功能简述〉<br>
 * 〈121. 买卖股票的最佳时机〉
 *
 * @author zhao
 * @date 2018/9/9 17:59
 * @since 1.0.1
 */
public class Solution121 {
  /**************************************
   * 题目
   给定一个数组，它的第 i 个元素是一支给定股票第 i 天的价格。

   如果你最多只允许完成一笔交易（即买入和卖出一支股票），设计一个算法来计算你所能获取的最大利润。

   注意你不能在买入股票前卖出股票。

   示例 1:

   输入: [7,1,5,3,6,4]
   输出: 5
   解释: 在第 2 天（股票价格 = 1）的时候买入，在第 5 天（股票价格 = 6）的时候卖出，最大利润 = 6-1 = 5 。
   注意利润不能是 7-1 = 6, 因为卖出价格需要大于买入价格。
   示例 2:

   输入: [7,6,4,3,1]
   输出: 0
   解释: 在这种情况下, 没有交易完成, 所以最大利润为 0。
   **************************************/

  /**************************************
   *
   * 想法：
   *    设置最小最大值
   *    判断
   * 我的做法
   * 超过88.68%的测试案例
   * 总结：
   *
   * ***********************************/
  public int maxProfit(int[] prices) {
    int min = Integer.MAX_VALUE;
    int max = 0;
    int result = 0;
    for (int i = 0; i < prices.length; i++) {
      int price = prices[i];
      result = price - min;

      min = Math.min(price, min);
      max = Math.max(price, max);

    }

    return 0;
  }

  /**************************************
   * 比我好的答案 better
   * ***********************************/
  public void better() {
  }

}
