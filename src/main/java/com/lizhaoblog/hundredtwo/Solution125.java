/*
 * Copyright (C), 2015-2018
 * FileName: Solution125
 * Author:   zhao
 * Date:     2018/9/9 21:11
 * Description: 125. 验证回文串
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.hundredtwo;

/**
 * 〈一句话功能简述〉<br>
 * 〈125. 验证回文串〉
 *
 * @author zhao
 * @date 2018/9/9 21:11
 * @since 1.0.1
 */
public class Solution125 {
  /**************************************
   * 题目
   给定一个字符串，验证它是否是回文串，只考虑字母和数字字符，可以忽略字母的大小写。

   说明：本题中，我们将空字符串定义为有效的回文串。

   示例 1:

   输入: "A man, a plan, a canal: Panama"
   输出: true
   示例 2:

   输入: "race a car"
   输出: false
   **************************************/

  /**************************************
   *
   * 想法：
   *    预处理数据
   *      将源字符串里面的非法字符都去掉
   *      然后进行遍历
   * 我的做法
   *      超过99.95%的测试案例
   * 总结：
   *
   * ***********************************/
  public boolean isPalindrome(String s) {
    char[] chars = s.toCharArray();
    char[] newChars = new char[chars.length];
    int count = 0;

    for (char a : chars) {
      // 0-9、A-Z、a-z、\
      // 空格 不算
      if (a >= 48 && a <= 57) {
      } else if (a >= 65 && a <= 90) {
      } else if (a >= 97 && a <= 122) {
      }
//      else if (a == 32) {
      //      }
      else {
        continue;
      }
      newChars[count++] = a;
    }

    boolean notSame = false;
    for (int i = 0; i < count / 2; i++) {
      if (notSame) {
        return false;
      }

      char charLeft = newChars[i];
      char charRight = newChars[count - i - 1];
      if (charLeft == charRight) {
        notSame = false;
        continue;
      } else if (charLeft >= 65 && charRight >= 65) {
        if ((charLeft - charRight) % 32 == 0) {
          notSame = false;
          continue;
        }
      }
      notSame = true;
    }

    if (notSame) {
      return false;
    }
    return true;
  }

  /**************************************
   * 比我好的答案 better
   * ***********************************/
  public void better() {
  }

}
