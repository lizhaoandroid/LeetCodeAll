/*
 * Copyright (C), 2015-2018
 * FileName: Solution155
 * Author:   zhao
 * Date:     2018/9/13 11:41
 * Description: 155. 最小栈
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.hundredtwo;

import com.lizhaoblog.diynode.MinStack;

/**
 * 〈一句话功能简述〉<br>
 * 〈155. 最小栈〉
 *
 * @author zhao
 * @date 2018/9/13 11:41
 * @since 1.0.1
 */
public class Solution155 {
  /**************************************
   * 题目
   设计一个支持 push，pop，top 操作，并能在常数时间内检索到最小元素的栈。

   push(x) -- 将元素 x 推入栈中。
   pop() -- 删除栈顶的元素。
   top() -- 获取栈顶元素。
   getMin() -- 检索栈中的最小元素。
   示例:

   MinStack minStack = new MinStack();
   minStack.push(-2);
   minStack.push(0);
   minStack.push(-3);
   minStack.getMin();   --> 返回 -3.
   minStack.pop();
   minStack.top();      --> 返回 0.
   minStack.getMin();   --> 返回 -2.
   **************************************/

  /**************************************
   *
   * 想法：
   *    1. 设计一个栈，常数时间获取最小值
   *    使用2个栈，一个A存正常数据，一个B存最小值
   *        当push进去的时候判断，是不是小于等于B现在的值
   *            是：往里面插入
   *            否：不插入
   *        pop的时候，需要判断是不是最小值被删除掉了
   *            是：同时删除
   * 我的做法
   *      超过30%的测试案例
   *
   * 代码执行过程：
   *
   * 总结：
   *      需要判断空指针
   *
   * ***********************************/
  public void example() {

  }

  public void test() {
    MinStack minStack = new MinStack();
    minStack.push(-2);
    minStack.push(0);
    minStack.push(-3);
    // --> 返回 -3.
    minStack.getMin();
    minStack.pop();
    // --> 返回 0.
    int top = minStack.top();
    // --> 返回 -2.
    minStack.getMin();
  }

  /**************************************
   * 比我好的答案 better
   * ***********************************/
  public void better() {
  }

}
