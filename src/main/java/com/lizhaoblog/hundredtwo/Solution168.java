/*
 * Copyright (C), 2015-2018
 * FileName: Solution168
 * Author:   zhao
 * Date:     2018/9/22 16:11
 * Description: 168. Excel表列名称
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.hundredtwo;

/**
 * 〈一句话功能简述〉<br>
 * 〈168. Excel表列名称〉
 *
 * @author zhao
 * @date 2018/9/22 16:11
 * @since 1.0.1
 */
public class Solution168 {
  /**************************************
   * 题目
   给定一个正整数，返回它在 Excel 表中相对应的列名称。

   例如，

   1 -> A
   2 -> B
   3 -> C
   ...
   26 -> Z
   27 -> AA
   28 -> AB
   ...
   示例 1:

   输入: 1
   输出: "A"
   示例 2:

   输入: 28
   输出: "AB"
   示例 3:

   输入: 701
   输出: "ZY"
   **************************************/

  /**************************************
   *
   * 想法：
   *  应该是一个二十六进制的数据
   *  无限对26取余数
   *      余数是结果、商是继续计算的条件
   *
   *      可以用十进制的逻辑思考
   *
   * 我的做法
   *      超过  %的测试案例
   *
   * 代码执行过程：
   *
   * 总结：
   *
   * ***********************************/
  public String convertToTitle(int n) {

    String result = "";
    while (n > 0) {
      int mod = (n - 1) % 26;
      n = (n - 1) / 26;
      result = (char) (mod + 'A') + result;
    }

    return result;
  }

  /**************************************
   * 比我好的答案 better
   * ***********************************/
  public void better() {
  }

}
