/*
 * Copyright (C), 2015-2018
 * FileName: Solution171
 * Author:   zhao
 * Date:     2018/9/23 15:28
 * Description: 171. Excel表列序号
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.hundredtwo;

/**
 * 〈一句话功能简述〉<br>
 * 〈171. Excel表列序号〉
 *
 * @author zhao
 * @date 2018/9/23 15:28
 * @since 1.0.1
 */
public class Solution171 {
  /**************************************
   * 题目
   给定一个Excel表格中的列名称，返回其相应的列序号。

   例如，

   A -> 1
   B -> 2
   C -> 3
   ...
   Z -> 26
   AA -> 27
   AB -> 28
   ...
   示例 1:

   输入: "A"
   输出: 1
   示例 2:

   输入: "AB"
   输出: 28
   示例 3:

   输入: "ZY"
   输出: 701
   **************************************/

  /**************************************
   *
   * 想法：
   *
   * 我的做法
   *      超过  %的测试案例
   *
   * 代码执行过程：
   *
   * 总结：
   *
   * ***********************************/
  public int titleToNumber(String s) {
    Double result = 0D;
    char[] chars = s.toCharArray();
    for (int i = chars.length; i > 0; i--) {
      int dif = chars[i - 1] - 'A' + 1;
      int exponent = chars.length - i;
      result += dif * Math.pow(26, exponent);
    }

    return result.intValue();
  }

  /**************************************
   * 比我好的答案 better
   * ***********************************/
  public void better() {
  }

}
