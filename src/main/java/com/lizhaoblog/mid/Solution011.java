/*
 * Copyright (C), 2015-2018
 * FileName: Solution011
 * Author:   zhao
 * Date:     2018/11/1 20:39
 * Description: 11. 盛最多水的容器
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.mid;

/**
 * 〈一句话功能简述〉<br>
 * 〈11. 盛最多水的容器〉
 *
 * @author zhao
 * @date 2018/11/1 20:39
 * @since 1.0.1
 */
public class Solution011 {
    /**************************************
     * 题目
     11. 盛最多水的容器
     题目描述提示帮助提交记录社区讨论阅读解答
     给定 n 个非负整数 a1，a2，...，an，每个数代表坐标中的一个点 (i, ai) 。
     在坐标内画 n 条垂直线，垂直线 i 的两个端点分别为 (i, ai) 和 (i, 0)。
     找出其中的两条线，使得它们与 x 轴共同构成的容器可以容纳最多的水。

     说明：你不能倾斜容器，且 n 的值至少为 2。
     图中垂直线代表输入数组 [1,8,6,2,5,4,8,3,7]。在此情况下，容器能够容纳水（表示为蓝色部分）的最大值为 49。

     示例:
     输入: [1,8,6,2,5,4,8,3,7]
     输出: 49
     **************************************/

    /**************************************
     *
     * 想法：
     *          1. 暴力破解
     *              2层循环，计算面积
     *              时间复杂度：O(n^2)
     *              空间复杂度：O(1)
     *          2.双指针法
     *              一个指针指向第一个数据
     *              一个指针指向最后一个数据
     *              算出面积，与已有面积对比
     *              将短的那条往长的那条靠拢
     *              返回结果
     *
     *              时间复杂度：O(n)
     *              空间复杂度：O(1)
     * 我的做法
     *      超过23%的测试案例
     *
     * 代码执行过程：
     *
     * 总结：
     *
     * ***********************************/

    public int maxArea(int[] height) {
        int maxVal = 0;
        int left = 0;
        int right = height.length - 1;

        while (left < right) {
            int dif = right - left;
            int leftVal = height[left];
            int rightVal = height[right];

            int value = calcArea(leftVal, rightVal, dif);
            maxVal = Math.max(maxVal, value);

            if (leftVal < rightVal) {
                left++;
            } else {
                right--;
            }
        }
        return maxVal;
    }

    /**
     * 暴力破解
     * 超过23%的测试案例
     *
     * @param height 数组
     * @return
     */
    public int maxAreaByBrute(int[] height) {
        int maxVal = 0;
        for (int i = 0; i < height.length; i++) {
            for (int j = i + 1; j < height.length; j++) {
                int dif = j - i;
                int value = calcArea(height[i], height[j], dif);
                maxVal = Math.max(maxVal, value);
            }
        }
        return maxVal;
    }

    private static int calcArea(int i, int j, int dif) {
        int min = Math.min(i, j);
        return min * dif;
    }

    /**************************************
     * 比我好的答案 better
     * ***********************************/
    public int better(int[] height) {
        // 声明变量
        int left = 0, right = height.length - 1, max = 0;
        while (left < right) {
            // 算出结果
            max = Math.max(Math.min(height[left], height[right]) * (right - left), max);
            if (height[left] < height[right]) {
                left++;
            } else {
                right--;
            }
        }
        return max;
    }

}
