/*
 * Copyright (C), 2015-2018
 * FileName: Solution022
 * Author:   zhao
 * Date:     2018/11/21 15:23
 * Description: 22. 括号生成
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.mid;

import java.util.Collections;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈22. 括号生成〉
 *
 * @author zhao
 * @date 2018/11/21 15:23
 * @since 1.0.1
 */
public class Solution022 {
    /**************************************
     * 题目
     给出 n 代表生成括号的对数，请你写出一个函数，使其能够生成所有可能的并且有效的括号组合。

     例如，给出 n = 3，生成结果为：

     [
     "((()))",
     "(()())",
     "(())()",
     "()(())",
     "()()()"
     ]
     **************************************/

    /**************************************
     *
     * 想法：
     *
     * 我的做法
     *      超过  %的测试案例
     *      时间复杂度：
     *      空间复杂度：
     * 代码执行过程：
     *
     * 总结：
     *
     * ***********************************/
    public List<String> generateParenthesis(int n) {
        if (n <= 0) {
            return Collections.EMPTY_LIST;
        }
        while (n > 0) {
            n--;
        }

        return Collections.EMPTY_LIST;
    }

    /**************************************
     * 比我好的答案 better
     * ***********************************/
    public void better() {
    }
}
