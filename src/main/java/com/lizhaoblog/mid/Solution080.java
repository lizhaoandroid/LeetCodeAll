/*
 * Copyright (C), 2015-2018
 * FileName: Solution080
 * Author:   zhao
 * Date:     2018/12/21 18:31
 * Description: 80. 删除排序数组中的重复项 II
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.mid;

/**
 * 〈一句话功能简述〉<br>
 * 〈80. 删除排序数组中的重复项 II〉
 *
 * @author zhao
 * @date 2018/12/21 18:31
 * @since 1.0.1
 */
public class Solution080 {
    /**************************************
     * 题目

     给定一个排序数组，你需要在原地删除重复出现的元素，使得每个元素最多出现两次，返回移除后数组的新长度。

     不要使用额外的数组空间，你必须在原地修改输入数组并在使用 O(1) 额外空间的条件下完成。

     示例 1:

     给定 nums = [1,1,1,2,2,3],

     函数应返回新长度 length = 5, 并且原数组的前五个元素被修改为 1, 1, 2, 2, 3 。

     你不需要考虑数组中超出新长度后面的元素。
     示例 2:

     给定 nums = [0,0,1,1,1,1,2,3,3],

     函数应返回新长度 length = 7, 并且原数组的前五个元素被修改为 0, 0, 1, 1, 2, 3, 3 。

     你不需要考虑数组中超出新长度后面的元素。
     说明:

     为什么返回数值是整数，但输出的答案是数组呢?

     请注意，输入数组是以“引用”方式传递的，这意味着在函数里修改输入数组对于调用者是可见的。

     你可以想象内部操作如下:

     // nums 是以“引用”方式传递的。也就是说，不对实参做任何拷贝
     int len = removeDuplicates(nums);

     // 在函数里修改输入数组对于调用者是可见的。
     // 根据你的函数返回的长度, 它会打印出数组中该长度范围内的所有元素。
     for (int i = 0; i < len; i++) {
     print(nums[i]);
     }
     **************************************/

    /*************************************
     想法：
     双指针法：第一个指针保证前面的数据是肯定不重复的，第二个指针指向当前位置
     当遇到下一个值的时候操作
     nums[index1] = nums[index2];
     判断：
     不相等：继续往下走，++index1，++index2，重置count
     相等：++index2，++count

     我的做法
     超过34%的测试案例
     时间复杂度/空间复杂度：n/1
     代码执行过程：

     总结：

     ************************************/
    public int removeDuplicates(int[] nums) {
        if (nums.length < 3) {
            return nums.length;
        }

        int index1 = 1;
        int index2 = 1;
        int preNum = nums[0];
        int count = 1;

        while (index2 < nums.length) {
            nums[index1] = nums[index2];
            if (nums[index2] != preNum) {
                preNum = nums[index2];
                count = 1;
                index1++;
            } else {
                count++;
                if (count <= 2) {
                    index1++;
                }
            }
            index2++;

        }

        return index1;
    }

    /**************************************
     * 比我好的答案 better
     * ***********************************/
    public int better(int[] nums) {
        /**
         for e in nums:
         if i < 2 or e != nums[i-2]:
         nums[i] = e
         i += 1
         return i
         */
        if (nums == null || nums.length == 0) {
            return 0;
        }
        if (nums.length == 1) {
            return 1;
        }
        int k = 0;
        for (int i = 0; i < nums.length; i++) {
            if (k < 2 || nums[i] != nums[k - 2]) {
                nums[k++] = nums[i];
            }
        }
        return k;
    }

}
