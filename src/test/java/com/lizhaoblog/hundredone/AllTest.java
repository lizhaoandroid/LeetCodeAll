/*
 * Copyright (C), 2015-2018
 * FileName: AllTest
 * Author:   zhao
 * Date:     2018/7/20 22:14
 * Description: 所有的测试
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.hundredone;

import com.lizhaoblog.diynode.ListNode;
import com.lizhaoblog.diynode.TreeNode;

import org.junit.Assert;
import org.junit.Test;

/**
 * 〈一句话功能简述〉<br>
 * 〈所有的测试〉
 *
 * @author zhao
 * @date 2018/7/20 22:14
 * @since 1.0.0
 */
public class AllTest {

  @Test
  public void testExample() {
    // 创建测试案例

    // 测试案例期望值

    // 执行方法

    // 判断期望值与实际值

  }

  @Test
  public void testCommon() {
    String[] test1 = new String[1];
    String[] test2 = new String[] { "flower", "flow", "flight" };
    String[] test3 = { "flower", "flow", "flight" };

    double x1 = 1.6;
    double x2 = 1.2;
    int expResult = 1;
    Assert.assertEquals(expResult, (int) x1);
    Assert.assertEquals(expResult, (int) x2);

    int i1 = 1;
    int i2 = 2;
    int result = (i1 + i2) / 2;
    System.out.print(result);
  }

  @Test
  public void test13() {
    String test1 = "IVII";
    int expResult = 6;
    Solution013 solution013 = new Solution013();

    int result1 = solution013.romanToInt(test1);
    int bResult1 = solution013.betterRomanToInt(test1);

    Assert.assertEquals(expResult, result1);
    Assert.assertEquals(expResult, bResult1);
  }

  @Test
  public void test14() {
    String[] test1 = new String[] { "flower", "flow", "flight" };

    String expResult1 = "fl";
    Solution014 solution14 = new Solution014();

    String result1 = solution14.longestCommonPrefix(test1);
    String bResult1 = solution14.better(test1);
    String bResult2 = solution14.better2(test1);

    Assert.assertEquals(expResult1, result1);
    Assert.assertEquals(expResult1, bResult1);
    Assert.assertEquals(expResult1, bResult2);
  }

  @Test
  public void test020() {
    String test1 = "()";
    String test2 = "()[]{}";
    String test3 = "(]";
    String test4 = "([)]";
    String test5 = "{[]}";

    boolean expResult1 = true;
    boolean expResult2 = true;
    boolean expResult3 = false;
    boolean expResult4 = false;
    boolean expResult5 = true;

    Solution020 solution020 = new Solution020();

    boolean result1 = solution020.isValid(test1);
    boolean result2 = solution020.isValid(test2);
    boolean result3 = solution020.isValid(test3);
    boolean result4 = solution020.isValid(test4);
    boolean result5 = solution020.isValid(test5);

    Assert.assertEquals(expResult1, result1);
    Assert.assertEquals(expResult2, result2);
    Assert.assertEquals(expResult3, result3);
    Assert.assertEquals(expResult4, result4);
    Assert.assertEquals(expResult5, result5);
  }

  @Test
  public void test021() {
    ListNode listNode11 = new ListNode(1);
    ListNode listNode12 = new ListNode(2);
    ListNode listNode13 = new ListNode(4);
    listNode11.next = listNode12;
    listNode12.next = listNode13;

    ListNode listNode21 = new ListNode(1);
    ListNode listNode22 = new ListNode(3);
    ListNode listNode23 = new ListNode(4);
    listNode21.next = listNode22;
    listNode22.next = listNode23;

    // 测试案例期望值
    ListNode expResult1 = new ListNode(1);
    ListNode expResult12 = new ListNode(1);
    ListNode expResult13 = new ListNode(2);
    ListNode expResult15 = new ListNode(3);
    ListNode expResult16 = new ListNode(4);
    ListNode expResult17 = new ListNode(4);
    expResult1.next = expResult12;
    expResult12.next = expResult13;
    expResult13.next = expResult15;
    expResult15.next = expResult16;
    expResult16.next = expResult17;

    Solution021 solution021 = new Solution021();

    ListNode result1 = solution021.mergeTwoLists(listNode11, listNode21);

    Assert.assertEquals(expResult1.toArray(), result1.toArray());
  }

  @Test
  public void test026() {
    final int[] test1 = new int[] { 1, 1, 2, 2 };
    final int[] test2 = new int[] { 1, 1, 2, 2, 4, 7, 8, 8 };
    final int[] test3 = new int[] { 1, 1 };

    int expResult1 = 2;
    int expResult2 = 5;
    int expResult3 = 1;
    Solution026 solution026 = new Solution026();

    int result1 = solution026.removeDuplicates(test1);
    int result2 = solution026.removeDuplicates(test2);
    int result3 = solution026.removeDuplicates(test3);
    int bResult1 = solution026.better(test1);
    int bResult2 = solution026.better(test2);
    int bResult3 = solution026.better(test3);

    Assert.assertEquals(expResult1, result1);
    Assert.assertEquals(expResult2, result2);
    Assert.assertEquals(expResult3, result3);
    Assert.assertEquals(expResult1, bResult1);
    Assert.assertEquals(expResult2, bResult2);
    Assert.assertEquals(expResult3, bResult3);
  }

  @Test
  public void test28() {
    String haystack = "hello";
    String needle = "ll";
    int expResult = 2;
    Solution28 solution28 = new Solution28();

    int result1 = solution28.strStr(haystack, needle);
    int bResult1 = solution28.better(haystack, needle);

    Assert.assertEquals(expResult, result1);
    Assert.assertEquals(expResult, bResult1);
  }

  @Test
  public void test058() {
    // 创建测试案例
    String str1 = "Hello World";
    String str2 = " ";

    // 测试案例期望值
    int expResult1 = 5;
    int expResult2 = 0;

    // 执行方法
    Solution058 solution058 = new Solution058();
    int result1 = solution058.lengthOfLastWord(str1);
    int result2 = solution058.lengthOfLastWord(str2);

    // 判断期望值与实际值
    Assert.assertEquals(expResult1, result1);
    Assert.assertEquals(expResult2, result2);
  }

  @Test
  public void test066() {
    // 创建测试案例
    int[] digits1 = new int[] { 1, 2, 3, 4 };
    int[] digits2 = new int[] { 9, 9, 9 };
    int[] digits3 = new int[] { 9 };

    // 测试案例期望值
    int[] expResult1 = new int[] { 1, 2, 3, 5 };
    int[] expResult2 = new int[] { 1, 0, 0, 0 };
    int[] expResult3 = new int[] { 1, 0 };

    // 执行方法
    Solution066 solution066 = new Solution066();
    int[] result1 = solution066.plusOne(digits1);
    int[] result2 = solution066.plusOne(digits2);
    int[] result3 = solution066.plusOne(digits3);

    // 判断期望值与实际值
    Assert.assertArrayEquals(expResult1, result1);
    Assert.assertArrayEquals(expResult2, result2);
    Assert.assertArrayEquals(expResult3, result3);
  }

  @Test
  public void test067() {
    // 创建测试案例
    String str1a = "11";
    String str1b = "1";

    // 测试案例期望值
    String expResult1 = "100";

    // 执行方法
    Solution067 solution067 = new Solution067();
    String result1 = solution067.addBinary(str1a, str1b);

    // 判断期望值与实际值
    Assert.assertEquals(expResult1, result1);
  }

  @Test
  public void test069() {
    // 创建测试案例
    int test1 = 795;

    // 测试案例期望值
    int expResult1 = 28;

    // 执行方法
    Solution069 solution069 = new Solution069();
    int result1 = solution069.mySqrt(test1);

    // 判断期望值与实际值
    Assert.assertEquals(expResult1, result1);
  }

  @Test
  public void test070() {
    int test1 = 3;

    int expResult1 = 3;
    Solution070 solution070 = new Solution070();

    int result1 = solution070.climbStairs(test1);

    Assert.assertEquals(expResult1, result1);
  }

  @Test
  public void test083() {
    Solution083 solution083 = new Solution083();
    solution083.testDeleteDuplicates();
  }

  @Test
  public void test088() {
    // 创建测试案例
    int[] test1Nums1 = new int[] { 1, 2, 3, 0, 0, 0 };
    int[] test1Nums2 = new int[] { 2, 5, 6 };
    int m = 3;
    int n = 3;

    // 测试案例期望值
    int[] expResult1 = new int[] { 1, 2, 2, 3, 5, 6 };

    // 执行方法
    Solution088 solution088 = new Solution088();
    solution088.merge(test1Nums1, m, test1Nums2, n);

    // 判断期望值与实际值
    Assert.assertArrayEquals(expResult1, test1Nums1);
  }

  @Test
  public void test0100() {
    // 创建测试案例
    TreeNode treeNode11 = new TreeNode(1);
    TreeNode treeNode12 = new TreeNode(2);
    TreeNode treeNode13 = new TreeNode(3);
    TreeNode treeNode21 = new TreeNode(1);
    TreeNode treeNode22 = new TreeNode(2);
    TreeNode treeNode23 = new TreeNode(3);
    treeNode11.left = treeNode12;
    treeNode11.right = treeNode13;
    treeNode21.left = treeNode22;
    treeNode21.right = treeNode23;

    // 测试案例期望值
    boolean expResult1 = true;

    // 执行方法
    Solution100 solution100 = new Solution100();
    boolean result = solution100.isSameTree(treeNode11, treeNode21);
    // 判断期望值与实际值
    Assert.assertEquals(expResult1, result);
  }

}