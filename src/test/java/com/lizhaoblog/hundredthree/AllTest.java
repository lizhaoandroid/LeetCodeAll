/*
 * Copyright (C), 2015-2018
 * FileName: AllTest
 * Author:   zhao
 * Date:     2018/9/25 11:42
 * Description: 200-300的测试案例
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.hundredthree;

import com.lizhaoblog.diynode.ListNode;
import com.lizhaoblog.hundredtwo.Solution171;

import org.junit.Assert;
import org.junit.Test;

/**
 * 〈一句话功能简述〉<br>
 * 〈200-300的测试案例〉
 *
 * @author zhao
 * @date 2018/9/25 11:42
 * @since 1.0.1
 */
public class AllTest {
  @Test
  public void test0206() {
    // 创建测试案例
    ListNode listNode1 = new ListNode(1);
    ListNode listNode2 = new ListNode(2);
    ListNode listNode3 = new ListNode(3);
    ListNode listNode4 = new ListNode(4);
    ListNode listNode5 = new ListNode(5);
    listNode1.next = listNode2;
    listNode2.next = listNode3;
    listNode3.next = listNode4;
    listNode4.next = listNode5;

    // 测试案例期望值
    Integer[] expResult1 = new Integer[] { 5, 4, 3, 2, 1 };

    // 执行方法
    Solution206 solution206 = new Solution206();
    ListNode result1 = solution206.reverseList(listNode1);

    // 判断结果.
    Assert.assertArrayEquals(expResult1, result1.toArray().toArray());

  }

  @Test
  public void test0217() {
    // 创建测试案例
    int[] nums1 = new int[] { 1, 2, 3, 1 };
    int[] nums2 = new int[] { 5, 4, 3, 2, 1 };
    // 测试案例期望值
    boolean expResult1 = true;
    boolean expResult2 = false;

    // 执行方法
    Solution217 solution217 = new Solution217();
    boolean result1 = solution217.containsDuplicate(nums1);
    boolean result2 = solution217.containsDuplicate(nums2);

    // 判断结果.
    Assert.assertEquals(expResult1, result1);
    Assert.assertEquals(expResult2, result2);

  }

  @Test
  public void test0219() {
    // 创建测试案例
    int[] nums1 = new int[] { 1, 2, 3, 1 };
    int[] nums2 = new int[] { 1, 1, 3, 2, 1 };
    int k1 = 3;
    int k2 = 4;

    // 测试案例期望值
    boolean expResult1 = true;
    boolean expResult2 = true;

    // 执行方法
    Solution219 solution219 = new Solution219();
    boolean result1 = solution219.containsNearbyDuplicate(nums1, k1);
    boolean result2 = solution219.containsNearbyDuplicate(nums2, k2);

    // 判断结果.
    Assert.assertEquals(expResult1, result1);
    Assert.assertEquals(expResult2, result2);

  }

  @Test
  public void test0234() {
    // 创建测试案例
    ListNode listNode1 = new ListNode(1);
    ListNode listNode2 = new ListNode(2);
    ListNode listNode3 = new ListNode(3);
    ListNode listNode4 = new ListNode(2);
    ListNode listNode5 = new ListNode(1);
    listNode1.next = listNode2;
    listNode2.next = listNode3;
    listNode3.next = listNode4;
    listNode4.next = listNode5;

    ListNode listNode21 = new ListNode(1);
    ListNode listNode22 = new ListNode(2);
    ListNode listNode23 = new ListNode(2);
    ListNode listNode24 = new ListNode(1);
    listNode21.next = listNode22;
    listNode22.next = listNode23;
    listNode23.next = listNode24;

    // 测试案例期望值
    boolean expResult1 = true;
    boolean expResult2 = true;

    // 执行方法
    Solution234 solution234 = new Solution234();
    boolean result1 = solution234.isPalindrome(listNode1);
    boolean result2 = solution234.isPalindrome(listNode21);

    // 判断结果.
    Assert.assertEquals(expResult1, result1);
    Assert.assertEquals(expResult2, result2);

  }

  @Test
  public void test0242() {
    // 创建测试案例
    String s11 = "anagram";
    String s12 = "nagaram";

    String s21 = "rat";
    String s22 = "car";

    // 测试案例期望值
    boolean expResult1 = true;
    boolean expResult2 = false;

    // 执行方法
    Solution242 solution242 = new Solution242();
    boolean result1 = solution242.isAnagram(s11, s12);
    boolean result2 = solution242.isAnagram(s21, s22);

    // 判断结果.
    Assert.assertEquals(expResult1, result1);
    Assert.assertEquals(expResult2, result2);
  }

  @Test
  public void test0237() {
    // 创建测试案例
    ListNode listNode1 = new ListNode(1);
    ListNode listNode2 = new ListNode(2);
    ListNode listNode3 = new ListNode(3);
    ListNode listNode4 = new ListNode(4);
    ListNode listNode5 = new ListNode(5);
    listNode1.next = listNode2;
    listNode2.next = listNode3;
    listNode3.next = listNode4;
    listNode4.next = listNode5;

    ListNode listNodeDel = new ListNode(4);

    // 测试案例期望值
    //    ListNode expResult1 = listNode1;
    Integer[] expResult1 = new Integer[] { 1, 2, 3, 5 };

    // 执行方法
    Solution237 solution237 = new Solution237();
    solution237.deleteNode(listNode1, listNodeDel);

    // 判断结果.
    Assert.assertArrayEquals(expResult1, listNode1.toArray().toArray());

  }

}
