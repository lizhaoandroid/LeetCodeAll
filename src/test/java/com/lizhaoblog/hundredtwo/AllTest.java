/*
 * Copyright (C), 2015-2018
 * FileName: AllTest
 * Author:   zhao
 * Date:     2018/9/9 13:15
 * Description: 一百到二百测试
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.hundredtwo;

import com.lizhaoblog.diynode.ListNode;
import com.lizhaoblog.diynode.MinStack;
import com.lizhaoblog.diynode.TreeNode;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈一百到二百测试〉
 *
 * @author zhao
 * @date 2018/9/9 13:15
 * @since 1.0.1
 */
public class AllTest {
  @Test
  public void test0101() {
    // 创建测试案例
    TreeNode treeNode11 = new TreeNode(1);
    TreeNode treeNode12 = new TreeNode(2);
    TreeNode treeNode13 = new TreeNode(2);

    TreeNode treeNode121 = new TreeNode(4);
    TreeNode treeNode122 = new TreeNode(5);
    TreeNode treeNode131 = new TreeNode(5);
    TreeNode treeNode132 = new TreeNode(4);

    treeNode11.left = treeNode12;
    treeNode11.right = treeNode13;
    treeNode12.left = treeNode121;
    treeNode12.right = treeNode122;
    treeNode13.left = treeNode131;
    treeNode13.right = treeNode132;

    // 测试案例期望值
    boolean expResult1 = true;

    // 执行方法
    Solution101 solution101 = new Solution101();
    boolean result = solution101.isSymmetric(treeNode11);
    // 判断期望值与实际值
    Assert.assertEquals(expResult1, result);
  }

  @Test
  public void test0104() {
    // 创建测试案例
    TreeNode treeNode11 = new TreeNode(1);
    TreeNode treeNode12 = new TreeNode(2);
    TreeNode treeNode13 = new TreeNode(2);

    TreeNode treeNode121 = new TreeNode(4);
    TreeNode treeNode122 = new TreeNode(5);
    TreeNode treeNode131 = new TreeNode(5);
    TreeNode treeNode132 = new TreeNode(4);

    treeNode11.left = treeNode12;
    treeNode11.right = treeNode13;
    treeNode12.left = treeNode121;
    treeNode12.right = treeNode122;
    treeNode13.left = treeNode131;
    treeNode13.right = treeNode132;

    // 测试案例期望值
    int expResult1 = 3;

    // 执行方法
    Solution104 solution104 = new Solution104();
    int result = solution104.maxDepth(treeNode11);
    // 判断期望值与实际值
    Assert.assertEquals(expResult1, result);
  }

  @Test
  public void test0107() {
    // 创建测试案例
    TreeNode treeNode11 = new TreeNode(1);
    TreeNode treeNode12 = new TreeNode(2);
    TreeNode treeNode13 = new TreeNode(2);

    TreeNode treeNode121 = new TreeNode(4);
    TreeNode treeNode122 = new TreeNode(5);
    TreeNode treeNode131 = new TreeNode(5);
    TreeNode treeNode132 = new TreeNode(4);

    treeNode11.left = treeNode12;
    treeNode11.right = treeNode13;
    treeNode12.left = treeNode121;
    treeNode12.right = treeNode122;
    treeNode13.left = treeNode131;
    treeNode13.right = treeNode132;

    // 测试案例期望值
    List<List<Integer>> expResult1 = new ArrayList<>();
    ArrayList<Integer> objects1 = new ArrayList<>();
    ArrayList<Integer> objects2 = new ArrayList<>();
    ArrayList<Integer> objects3 = new ArrayList<>();
    objects1.add(1);
    objects2.add(2);
    objects2.add(2);
    objects3.add(4);
    objects3.add(5);
    objects3.add(5);
    objects3.add(4);
    expResult1.add(objects3);
    expResult1.add(objects2);
    expResult1.add(objects1);

    // 执行方法
    Solution107 solution107 = new Solution107();
    List<List<Integer>> result = solution107.levelOrderBottom(treeNode11);
    // 判断期望值与实际值
    Assert.assertArrayEquals(expResult1.toArray(), result.toArray());
  }

  @Test
  public void test0111() {
    // 创建测试案例
    TreeNode treeNode11 = new TreeNode(1);
    TreeNode treeNode12 = new TreeNode(2);
    TreeNode treeNode13 = new TreeNode(2);

    TreeNode treeNode121 = new TreeNode(4);
    TreeNode treeNode122 = new TreeNode(5);
    TreeNode treeNode131 = new TreeNode(5);
    TreeNode treeNode132 = new TreeNode(4);

    treeNode11.left = treeNode12;
    treeNode11.right = treeNode13;
    treeNode12.left = treeNode121;
    treeNode12.right = treeNode122;
    treeNode13.left = treeNode131;
    treeNode13.right = treeNode132;

    // 测试案例期望值
    int expResult1 = 3;

    // 执行方法
    Solution111 solution111 = new Solution111();
    int result = solution111.minDepth(treeNode11);
    // 判断期望值与实际值
    Assert.assertEquals(expResult1, result);
  }

  @Test
  public void test0112() {
    // 创建测试案例
    TreeNode treeNode11 = new TreeNode(1);
    TreeNode treeNode12 = new TreeNode(2);
    TreeNode treeNode13 = new TreeNode(2);

    TreeNode treeNode121 = new TreeNode(4);
    TreeNode treeNode122 = new TreeNode(5);
    TreeNode treeNode131 = new TreeNode(5);
    TreeNode treeNode132 = new TreeNode(4);

    treeNode11.left = treeNode12;
    treeNode11.right = treeNode13;
    treeNode12.left = treeNode121;
    treeNode12.right = treeNode122;
    treeNode13.left = treeNode131;
    treeNode13.right = treeNode132;

    int sum1 = 7;
    int sum2 = 9;

    // 测试案例期望值
    boolean expResult1 = true;
    boolean expResult2 = false;

    // 执行方法
    Solution112 solution112 = new Solution112();
    boolean result1 = solution112.hasPathSum(treeNode11, sum1);
    boolean result2 = solution112.hasPathSum(treeNode11, sum2);
    // 判断期望值与实际值
    Assert.assertEquals(expResult1, result1);
    Assert.assertEquals(expResult2, result2);
  }

  @Test
  public void test0125() {
    // 创建测试案例
    String str1 = "A man, a plan, a canal: Panama";
    String str2 = ".,";
    String str3 = "race a car";

    // 测试案例期望值
    boolean expResult1 = true;
    boolean expResult2 = true;
    boolean expResult3 = false;

    // 执行方法
    Solution125 solution125 = new Solution125();
    boolean result1 = solution125.isPalindrome(str1);
    boolean result2 = solution125.isPalindrome(str2);
    boolean result3 = solution125.isPalindrome(str3);
    // 判断期望值与实际值
    Assert.assertEquals(expResult1, result1);
    Assert.assertEquals(expResult2, result2);
    Assert.assertEquals(expResult3, result3);
  }

  @Test
  public void test0136() {
    // 创建测试案例
    int[] nums = new int[] { 2, 3, 4, 2, 4 };

    // 测试案例期望值
    int expResult1 = 3;

    // 执行方法
    Solution136 solution136 = new Solution136();
    int result1 = solution136.singleNumber(nums);
    // 判断期望值与实际值
    Assert.assertEquals(expResult1, result1);
  }

  @Test
  public void test0141() {
    // 创建测试案例
    ListNode listNode1 = new ListNode(1);
    ListNode listNode2 = new ListNode(2);
    ListNode listNode3 = new ListNode(3);
    ListNode listNode4 = new ListNode(4);
    ListNode listNode5 = new ListNode(5);
    listNode1.next = listNode2;
    listNode2.next = listNode3;
    listNode3.next = listNode4;
    listNode4.next = listNode5;

    // 测试案例期望值
    boolean expResult1 = false;

    // 执行方法
    Solution141 solution141 = new Solution141();
    boolean result1 = solution141.hasCycle(listNode1);
    // 判断期望值与实际值
    Assert.assertEquals(expResult1, result1);

    boolean expResult2 = true;
    listNode5.next = listNode3;
    // 判断期望值与实际值
    Assert.assertEquals(expResult1, result1);

  }

  @Test
  public void test0155() {
    // 创建测试案例

    // 测试案例期望值
    int expResult1 = -3;
    int expResult2 = 0;
    int expResult3 = -2;

    // 执行方法
    MinStack minStack = new MinStack();
    minStack.push(-2);
    minStack.push(0);
    minStack.push(-3);
    // --> 返回 -3.
    int min = minStack.getMin();
    Assert.assertEquals(expResult1, min);

    minStack.pop();
    // --> 返回 0.
    int top = minStack.top();
    Assert.assertEquals(expResult2, top);
    // --> 返回 -2.
    min = minStack.getMin();
    Assert.assertEquals(expResult3, min);
  }

  @Test
  public void test0160() {
    // 创建测试案例
    ListNode listNode1 = new ListNode(1);
    ListNode listNode2 = new ListNode(2);
    ListNode listNode3 = new ListNode(3);
    ListNode listNode4 = new ListNode(222);
    ListNode listNode5 = new ListNode(223);
    listNode1.next = listNode2;
    listNode2.next = listNode3;
    listNode3.next = listNode4;
    listNode4.next = listNode5;

    ListNode listNode21 = new ListNode(26);
    ListNode listNode22 = new ListNode(28);
    ListNode listNode23 = new ListNode(29);
    listNode21.next = listNode22;
    listNode22.next = listNode23;
    listNode23.next = listNode4;

    // 测试案例期望值
    ListNode expResult1 = listNode4;

    // 执行方法
    Solution160 solution160 = new Solution160();
    ListNode intersectionNode = solution160.getIntersectionNode(listNode1, listNode21);
    // 判断结果.
    Assert.assertEquals(expResult1, intersectionNode);

  }

  @Test
  public void test0167() {
    // 创建测试案例
    int[] numbers = new int[] { 2, 3, 4 };
    int[] numbers2 = new int[] { 3, 24, 50, 79, 88, 150, 345 };
    int target = 6;
    int target2 = 200;

    // 测试案例期望值
    int[] expResult1 = new int[] { 1, 3 };
    int[] expResult2 = new int[] { 3, 6 };

    // 执行方法
    Solution167 solution167 = new Solution167();
    int[] result1 = solution167.twoSum(numbers, target);
    int[] result2 = solution167.twoSum(numbers2, target2);
    // 判断结果.
    Assert.assertArrayEquals(expResult1, result1);
    Assert.assertArrayEquals(expResult2, result2);

  }

  @Test
  public void test0168() {
    // 创建测试案例
    int n1 = 1;
    int n2 = 28;
    int n3 = 701;

    // 测试案例期望值
    String expResult1 = "A";
    String expResult2 = "AB";
    String expResult3 = "ZY";

    // 执行方法
    Solution168 solution168 = new Solution168();
    String result1 = solution168.convertToTitle(n1);
    String result2 = solution168.convertToTitle(n2);
    String result3 = solution168.convertToTitle(n3);

    // 判断结果.
    Assert.assertEquals(expResult1, result1);
    Assert.assertEquals(expResult2, result2);
    Assert.assertEquals(expResult3, result3);

  }

  @Test
  public void test0169() {
    // 创建测试案例
    int[] nums1 = new int[] { 3, 2, 3 };
    int[] nums2 = new int[] { 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024,
            1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024 };

    // 测试案例期望值
    int expResult1 = 3;
    int expResult2 = 2048;

    // 执行方法
    Solution169 solution169 = new Solution169();
    int result1 = solution169.majorityElement(nums1);
    int result2 = solution169.majorityElement(nums2);

    // 判断结果.
    Assert.assertEquals(expResult1, result1);
    Assert.assertEquals(expResult2, result2);

  }


  @Test
  public void test0171() {
    // 创建测试案例
    String n1 = "A";
    String n2 =  "AB";
    String n3 = "ZY";

    // 测试案例期望值
    int expResult1 =1;
    int expResult2 = 28;
    int expResult3 = 701;

    // 执行方法
    Solution171 solution171 = new Solution171();
    int result1 = solution171.titleToNumber(n1);
    int result2 = solution171.titleToNumber(n2);
    int result3 = solution171.titleToNumber(n3);

    // 判断结果.
    Assert.assertEquals(expResult1, result1);
    Assert.assertEquals(expResult2, result2);
    Assert.assertEquals(expResult3, result3);

  }

}
