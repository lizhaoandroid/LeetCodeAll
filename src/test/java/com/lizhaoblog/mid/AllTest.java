/*
 * Copyright (C), 2015-2018
 * FileName: AllTest
 * Author:   zhao
 * Date:     2018/10/8 13:08
 * Description: 中等题目的测试
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.mid;

import com.lizhaoblog.diynode.ListNode;
import com.lizhaoblog.hundredone.Solution013;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈中等题目的测试〉
 *
 * @author zhao
 * @date 2018/10/8 13:08
 * @since 1.0.1
 */
public class AllTest {
    @Test
    public void test002() {
        // 创建测试案例
        ListNode listNode1 = new ListNode(5);
        ListNode listNode2 = new ListNode(6);
        ListNode listNode3 = new ListNode(4);
        listNode1.next = listNode2;
        listNode2.next = listNode3;

        ListNode listNode21 = new ListNode(2);
        ListNode listNode22 = new ListNode(4);
        ListNode listNode23 = new ListNode(3);
        listNode21.next = listNode22;
        listNode22.next = listNode23;

        ListNode listNode31 = new ListNode(1);
        ListNode listNode32 = new ListNode(1);

        // 测试案例期望值
        Integer[] expResult1 = new Integer[] { 7, 0, 8 };
        Integer[] expResult3 = new Integer[] { 2 };

        // 执行方法
        Solution2 solution2 = new Solution2();
        ListNode result1 = solution2.addTwoNumbers(listNode1, listNode21);
        ListNode result3 = solution2.addTwoNumbers(listNode31, listNode32);
        // 判断期望值与实际值
        Assert.assertArrayEquals(expResult1, result1.toArray().toArray());
        Assert.assertArrayEquals(expResult3, result3.toArray().toArray());

    }

    @Test
    public void test003() {
        // 创建测试案例
        String s1 = "abcabcbb";
        String s2 = "bbbbb";
        String s3 = "pwwkew";

        // 测试案例期望值
        int expResult1 = 3;
        int expResult2 = 1;
        int expResult3 = 3;

        // 执行方法
        Solution3 solution3 = new Solution3();
        int result1 = solution3.lengthOfLongestSubstring(s1);
        int result2 = solution3.lengthOfLongestSubstring(s2);
        int result3 = solution3.lengthOfLongestSubstring(s3);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);
        Assert.assertEquals(expResult2, result2);
        Assert.assertEquals(expResult3, result3);

    }

    @Test
    public void test005() {
        // 创建测试案例
        String s1 = "babad";
        String s2 = "cbbd";
        String s3 = "pwwkew";

        // 测试案例期望值
        String expResult1 = "bab";
        String expResult2 = "bb";
        String expResult3 = "ww";

        // 执行方法
        Solution5 solution5 = new Solution5();
        String result1 = solution5.longestPalindromeByDP(s1);
        String result2 = solution5.longestPalindromeByDP(s2);
        String result3 = solution5.longestPalindromeByDP(s3);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);
        Assert.assertEquals(expResult2, result2);
        Assert.assertEquals(expResult3, result3);

    }

    @Test
    public void test011() {
        // 创建测试案例
        int[] arr1 = new int[] { 1, 8, 6, 2, 5, 4, 8, 3, 7 };

        // 测试案例期望值
        int expResult1 = 49;

        // 执行方法
        Solution011 solution011 = new Solution011();
        int result1 = solution011.maxArea(arr1);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);

    }

    @Test
    public void test012() {
        // 创建测试案例
        int s1 = 3;
        int s2 = 2345;

        // 测试案例期望值
        String expResult1 = "III";
        String expResult2 = "MMCCCXLV";

        // 执行方法
        Solution012 solution012 = new Solution012();
        String result1 = solution012.intToRoman(s1);
        String result2 = solution012.intToRoman(s2);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);
        Assert.assertEquals(expResult2, result2);

    }

    @Test
    public void test015() {
        // 创建测试案例
        int[] arr1 = new int[] { -1, 0, 1, 2, -1, -4 };

        // 测试案例期望值
        List<List<Integer>> expResult1 = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>();
        list1.add(-1);
        list1.add(0);
        list1.add(1);
        List<Integer> list2 = new ArrayList<>();
        list2.add(-1);
        list2.add(-1);
        list2.add(2);
        expResult1.add(list1);
        expResult1.add(list2);

        // 执行方法
        Solution015 solution015 = new Solution015();
        List<List<Integer>> result1 = solution015.threeSum(arr1);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);

    }

    @Test
    public void test016() {
        // 创建测试案例
        int[] arr1 = new int[] { -1, 2, 1, -4 };
        int target1 = 1;

        // 测试案例期望值
        int expResult1 = 2;

        // 执行方法
        Solution016 solution016 = new Solution016();
        int result1 = solution016.threeSumClosest(arr1, target1);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);

    }

    @Test
    public void test017() {
        // 创建测试案例
        String str1 = "23";

        // 测试案例期望值
        String[] strings = new String[] { "ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf" };
        List<String> expResult1 = Arrays.asList(strings);
        //        expResult1.addAll(strings);

        // 执行方法
        Solution017 solution017 = new Solution017();
        List<String> result1 = solution017.letterCombinations(str1);
        List<String> result11 = solution017.better(str1);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);
        Assert.assertEquals(expResult1, result11);

    }

    @Test
    public void test018() {
        // 创建测试案例
        //        int[] arr1 = new int[] { 1, 0, -1, 0, -2, 2 };
        int[] arr1 = new int[] { 0, 0, 0, 0 };
        int target1 = 0;

        // 测试案例期望值
        List<List<Integer>> expResult1 = new ArrayList<>();

        List<Integer> list1 = new ArrayList<>();
        list1.add(-1);
        list1.add(0);
        list1.add(0);
        list1.add(1);
        List<Integer> list2 = new ArrayList<>();
        list2.add(-2);
        list2.add(-1);
        list2.add(1);
        list2.add(2);
        List<Integer> list3 = new ArrayList<>();
        list2.add(-2);
        list2.add(0);
        list2.add(0);
        list2.add(2);
        expResult1.add(list1);
        expResult1.add(list2);
        expResult1.add(list3);

        // 执行方法
        Solution018 solution018 = new Solution018();
        List<List<Integer>> result1 = solution018.fourSum(arr1, target1);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);

    }

    @Test
    public void test019() {
        // 创建测试案例
        ListNode listNode1 = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(3);
        ListNode listNode4 = new ListNode(4);
        ListNode listNode5 = new ListNode(5);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        listNode4.next = listNode5;
        int n1 = 2;

        // 测试案例期望值
        ListNode expResult1 = new ListNode(1);
        ListNode expResult12 = new ListNode(2);
        ListNode expResult13 = new ListNode(3);
        ListNode expResult15 = new ListNode(5);
        expResult1.next = expResult12;
        expResult12.next = expResult13;
        expResult13.next = expResult15;

        // 执行方法
        Solution019 solution019 = new Solution019();
        ListNode result1 = solution019.removeNthFromEnd(listNode1, n1);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1.toArray(), result1.toArray());
    }

    @Test
    public void test023() {
        // 创建测试案例
        ListNode listNode11 = new ListNode(1);
        ListNode listNode12 = new ListNode(2);
        ListNode listNode13 = new ListNode(4);
        listNode11.next = listNode12;
        listNode12.next = listNode13;

        ListNode listNode21 = new ListNode(1);
        ListNode listNode22 = new ListNode(3);
        ListNode listNode23 = new ListNode(4);
        listNode21.next = listNode22;
        listNode22.next = listNode23;

        ListNode[] nodeList1 = new ListNode[] { listNode11, listNode21 };

        // 测试案例期望值
        ListNode expResult1 = new ListNode(1);
        ListNode expResult12 = new ListNode(1);
        ListNode expResult13 = new ListNode(2);
        ListNode expResult15 = new ListNode(3);
        ListNode expResult16 = new ListNode(4);
        ListNode expResult17 = new ListNode(4);
        expResult1.next = expResult12;
        expResult12.next = expResult13;
        expResult13.next = expResult15;
        expResult15.next = expResult16;
        expResult16.next = expResult17;

        Solution023 solution023 = new Solution023();

        ListNode result1 = solution023.mergeKLists(nodeList1);

        Assert.assertEquals(expResult1.toArray(), result1.toArray());
    }

    @Test
    public void test024() {
        // 创建测试案例
        ListNode listNode11 = new ListNode(1);
        ListNode listNode12 = new ListNode(2);
        ListNode listNode13 = new ListNode(3);
        ListNode listNode14 = new ListNode(4);
        listNode11.next = listNode12;
        listNode12.next = listNode13;
        listNode13.next = listNode14;

        // 测试案例期望值
        ListNode expResult1 = new ListNode(2);
        ListNode expResult12 = new ListNode(1);
        ListNode expResult13 = new ListNode(4);
        ListNode expResult15 = new ListNode(3);
        expResult1.next = expResult12;
        expResult12.next = expResult13;
        expResult13.next = expResult15;

        Solution024 solution024 = new Solution024();

        ListNode result1 = solution024.swapPairs(listNode11);

        Assert.assertEquals(expResult1.toArray(), result1.toArray());
    }

    @Test
    public void test029() {
        // 创建测试案例
        int dividend1 = 10;
        int divisor1 = 3;
        int dividend2 = 7;
        int divisor2 = -3;

        int expResult1 = 3;
        int expResult2 = -2;

        Solution029 solution029 = new Solution029();

        int result1 = solution029.divide(dividend1, divisor1);
        int result2 = solution029.divide(dividend2, divisor2);

        Assert.assertEquals(expResult1, result1);
        Assert.assertEquals(expResult2, result2);
    }

    @Test
    public void test041() {
        // 创建测试案例
        int[] test1 = new int[] { 0, 1, 2 };
        int[] test2 = new int[] { 10, 4, 16, 54, 17, -7, 21, 15, 25, 31, 61, 1, 6, 12, 21, 46, 16, 56, 54, 12, 23, 20,
                38, 63, 2, 27, 35, 11, 13, 47, 13, 11, 61, 39, 0, 14, 42, 8, 16, 54, 50, 12, -10, 43, 11, -1, 24, 38,
                -10, 13, 60, 0, 44, 11, 50, 33, 48, 20, 31, -4, 2, 54, -6, 51, 6 };
        int[] test3 = new int[] { 0, 1, 2, 3, 5 };
        int[] test4 = new int[] { 2 };

        // 测试案例期望值
        int expResult1 = 3;
        int expResult2 = 3;
        int expResult3 = 4;
        int expResult4 = 1;

        Solution041 solution041 = new Solution041();

        int result1 = solution041.firstMissingPositive(test1);
        int result2 = solution041.firstMissingPositive(test2);
        int result3 = solution041.firstMissingPositive(test3);
        int result4 = solution041.firstMissingPositive(test4);

        Assert.assertEquals(expResult1, result1);
        Assert.assertEquals(expResult2, result2);
        Assert.assertEquals(expResult3, result3);
        Assert.assertEquals(expResult4, result4);
    }

    @Test
    public void test046() {
        // 创建测试案例
        int[] arr1 = new int[] { 1, 2, 3 };

        // 测试案例期望值
        List<List<Integer>> expResult1 = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);
        List<Integer> list2 = new ArrayList<>();
        list2.add(1);
        list2.add(3);
        list2.add(2);
        List<Integer> list3 = new ArrayList<>();
        list3.add(2);
        list3.add(1);
        list3.add(3);
        List<Integer> list4 = new ArrayList<>();
        list4.add(2);
        list4.add(3);
        list4.add(1);
        List<Integer> list5 = new ArrayList<>();
        list5.add(3);
        list5.add(1);
        list5.add(2);
        List<Integer> list6 = new ArrayList<>();
        list6.add(3);
        list6.add(2);
        list6.add(1);
        expResult1.add(list1);
        expResult1.add(list2);
        expResult1.add(list3);
        expResult1.add(list4);
        expResult1.add(list5);
        expResult1.add(list6);

        // 执行方法
        Solution046 solution046 = new Solution046();
        List<List<Integer>> result1 = solution046.permute(arr1);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);

    }

    @Test
    public void test047() {
        // 创建测试案例
        int[] arr1 = new int[] { 1, 1, 2 };

        // 测试案例期望值
        List<List<Integer>> expResult1 = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(1);
        list1.add(2);
        List<Integer> list2 = new ArrayList<>();
        list2.add(1);
        list2.add(2);
        list2.add(1);
        List<Integer> list3 = new ArrayList<>();
        list3.add(2);
        list3.add(1);
        list3.add(1);
        expResult1.add(list1);
        expResult1.add(list2);
        expResult1.add(list3);

        // 执行方法
        Solution047 solution047 = new Solution047();
        List<List<Integer>> result1 = solution047.permuteUnique(arr1);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);

    }

    @Test
    public void test049() {
        // 创建测试案例
        String[] arr1 = new String[] { "eat", "tea", "tan", "ate", "nat", "bat" };

        // 测试案例期望值
        List<List<String>> expResult1 = new ArrayList<>();
        List<String> list1 = new ArrayList<>();
        list1.add("ate");
        list1.add("eat");
        list1.add("tea");
        List<String> list2 = new ArrayList<>();
        list2.add("nat");
        list2.add("tan");
        List<String> list3 = new ArrayList<>();
        list3.add("bat");
        expResult1.add(list1);
        expResult1.add(list2);
        expResult1.add(list3);

        // 执行方法
        Solution049 solution049 = new Solution049();
        List<List<String>> result1 = solution049.groupAnagrams(arr1);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);

    }

    @Test
    public void test060() {
        // 创建测试案例
        int n1 = 3;
        int k1 = 3;
        int n2 = 3;
        int k2 = 6;

        // 测试案例期望值
        String expResult1 = "213";
        String expResult2 = "321";

        // 执行方法
        Solution060 solution060 = new Solution060();
        String result1 = solution060.getPermutation(n1, k1);
        String result2 = solution060.getPermutation(n2, k2);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);
        Assert.assertEquals(expResult2, result2);

    }

    @Test
    public void test062() {
        // 创建测试案例
        int m1 = 3;
        int n1 = 2;
        int m2 = 7;
        int n2 = 3;

        // 测试案例期望值
        int expResult1 = 3;
        int expResult2 = 28;

        // 执行方法
        Solution062 solution062 = new Solution062();
        int result1 = solution062.uniquePaths(m1, n1);
        int result2 = solution062.uniquePaths(m2, n2);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);
        Assert.assertEquals(expResult2, result2);

    }

    @Test
    public void test063() {
        // 创建测试案例
        int[][] arr1 = new int[3][3];
        arr1[0][0] = 0;
        arr1[0][1] = 0;
        arr1[0][2] = 0;
        arr1[1][0] = 0;
        arr1[1][1] = 1;
        arr1[1][2] = 0;
        arr1[2][0] = 0;
        arr1[2][1] = 0;
        arr1[2][2] = 0;
        int[][] arr2 = new int[1][1];
        arr2[0][0] = 1;

        // 测试案例期望值
        int expResult1 = 2;
        int expResult2 = 0;

        // 执行方法
        Solution063 solution063 = new Solution063();
        int result1 = solution063.uniquePathsWithObstacles(arr1);
        int result2 = solution063.uniquePathsWithObstacles(arr2);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);
        Assert.assertEquals(expResult2, result2);

    }

    @Test
    public void test075() {
        // 创建测试案例

        int[] arr1 = new int[] { 2, 0, 2, 1, 1, 0 };

        // 测试案例期望值
        int[] expResult1 = new int[] { 0, 0, 1, 1, 2, 2 };

        // 执行方法
        Solution075 solution075 = new Solution075();
        solution075.sortColors(arr1);

        // 判断期望值与实际值
        Assert.assertArrayEquals(expResult1, arr1);
    }

    @Test
    public void test077() {
        // 创建测试案例
        int n1 = 4;
        int k1 = 2;

        // 测试案例期望值
        List<List<Integer>> expResult1 = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>();
        list1.add(2);
        list1.add(4);
        List<Integer> list2 = new ArrayList<>();
        list2.add(3);
        list2.add(4);
        List<Integer> list3 = new ArrayList<>();
        list3.add(2);
        list3.add(3);
        List<Integer> list4 = new ArrayList<>();
        list4.add(1);
        list4.add(2);
        List<Integer> list5 = new ArrayList<>();
        list5.add(1);
        list5.add(3);
        List<Integer> list6 = new ArrayList<>();
        list6.add(1);
        list6.add(4);
        expResult1.add(list1);
        expResult1.add(list2);
        expResult1.add(list3);
        expResult1.add(list4);
        expResult1.add(list5);
        expResult1.add(list6);

        // 执行方法
        Solution077 solution077 = new Solution077();
        List<List<Integer>> result1 = solution077.combine(n1, k1);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);
    }

    @Test
    public void test078() {
        // 创建测试案例
        int[] nums1 = new int[] { 1, 2, 3 };

        // 测试案例期望值
        List<List<Integer>> expResult1 = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>();
        list1.add(1);
        List<Integer> list2 = new ArrayList<>();
        list2.add(2);
        List<Integer> list3 = new ArrayList<>();
        list3.add(3);
        List<Integer> list4 = new ArrayList<>();
        list4.add(1);
        list4.add(2);
        List<Integer> list5 = new ArrayList<>();
        list5.add(1);
        list5.add(3);
        List<Integer> list6 = new ArrayList<>();
        list6.add(2);
        list6.add(3);
        List<Integer> list7 = new ArrayList<>();
        list7.add(1);
        list7.add(2);
        list7.add(3);
        List<Integer> list8 = new ArrayList<>();
        expResult1.add(list1);
        expResult1.add(list2);
        expResult1.add(list3);
        expResult1.add(list4);
        expResult1.add(list5);
        expResult1.add(list6);
        expResult1.add(list7);
        expResult1.add(list8);

        // 执行方法
        Solution078 solution078 = new Solution078();
        List<List<Integer>> result1 = solution078.subsets(nums1);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);
    }

    @Test
    public void test074() {
        // 创建测试案例
        int[][] arr1 = new int[3][4];
        arr1[0][0] = 1;
        arr1[0][1] = 3;
        arr1[0][2] = 5;
        arr1[0][3] = 7;
        arr1[1][0] = 10;
        arr1[1][1] = 11;
        arr1[1][2] = 16;
        arr1[1][3] = 20;
        arr1[2][0] = 23;
        arr1[2][1] = 30;
        arr1[2][2] = 34;
        arr1[2][3] = 50;
        int target1 = 16;
        int target2 = 13;

        int[][] arr2 = new int[1][2];
        arr2[0][0] = 1;
        arr2[0][1] = 3;
        int target3 = 3;

        // 测试案例期望值
        boolean expResult1 = true;
        boolean expResult2 = false;
        boolean expResult3 = true;

        // 执行方法
        Solution074 solution074 = new Solution074();
        boolean result1 = solution074.searchMatrix(arr1, target1);
        boolean result2 = solution074.searchMatrix(arr1, target2);
        boolean result3 = solution074.searchMatrix(arr2, target3);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);
        Assert.assertEquals(expResult2, result2);
        Assert.assertEquals(expResult3, result3);

    }

    @Test
    public void test080() {
        // 创建测试案例
        int[] arr1 = new int[] { 1, 1, 1, 2, 2, 3 };
        int[] arr2 = new int[] { 0, 0, 1, 1, 1, 1, 2, 3, 3 };

        // 测试案例期望值
        int expResult1 = 5;
        int expResult2 = 7;

        // 执行方法
        Solution080 solution080 = new Solution080();
        int result1 = solution080.removeDuplicates(arr1);
        int result2 = solution080.removeDuplicates(arr2);

        // 判断期望值与实际值
        Assert.assertEquals(expResult1, result1);
        Assert.assertEquals(expResult2, result2);
    }

    @Test
    public void test086() {
        // 创建测试案例
        ListNode listNode1 = new ListNode(1);
        ListNode listNode2 = new ListNode(4);
        ListNode listNode3 = new ListNode(3);
        ListNode listNode4 = new ListNode(2);
        ListNode listNode5 = new ListNode(5);
        ListNode listNode6 = new ListNode(2);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        listNode4.next = listNode5;
        listNode5.next = listNode6;
        int x1 = 3;

        // 测试案例期望值
        ListNode expResult11 = new ListNode(1);
        ListNode expResult12 = new ListNode(2);
        ListNode expResult13 = new ListNode(2);
        ListNode expResult14 = new ListNode(4);
        ListNode expResult15 = new ListNode(3);
        ListNode expResult16 = new ListNode(5);
        expResult11.next = expResult12;
        expResult12.next = expResult13;
        expResult13.next = expResult14;
        expResult14.next = expResult15;
        expResult15.next = expResult16;

        // 执行方法
        Solution086 solution086 = new Solution086();
        ListNode result1 = solution086.partition(listNode1, x1);

        // 判断期望值与实际值
        Assert.assertEquals(expResult11.toArray(), result1.toArray());
    }
}
